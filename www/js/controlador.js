angular.module('starter').controller('CalcController', function(MathService) {
//Función que calcula el cuadrado de un número haciendo uso del servicio
//de multiplicacion definido en "MathService"
	
  this.cuadrado = function() {
      this.result = MathService.multiplicar(this.number,this.number);
   }
});